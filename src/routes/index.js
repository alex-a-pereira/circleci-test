const { Router } = require('express');

const stuffRouter = require('./stuff')

const rootRouter = new Router();

rootRouter.use('/', stuffRouter);

module.exports = rootRouter;
