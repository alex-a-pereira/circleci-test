const { Router } = require('express');

const stuffRouter = new Router();

stuffRouter.all('/*', async (req, res) => {
  res.send(JSON.stringify({
    'itworked': true
  }))
})

module.exports = stuffRouter;
