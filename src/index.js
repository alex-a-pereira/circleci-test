var express = require('express')

var app = express()

const router = require('./routes')

app.use('/', router)

app.listen(5150);
