const multiply = (a, b) => {
  return a * b
}


const aPassingTest = () => {
  let result = multiply(2, 4)
  if(result !== 8) {
    throw Error('it failed!')
  } else {
    console.log('the first test passed')
  }
}


const aFailingTest = () => {
  let result = multiply(3, 4)
  if(result !== 12) {
    throw Error('it failed!')
  } else {
    console.log('the test passed')
  }
}

aPassingTest()
aFailingTest()